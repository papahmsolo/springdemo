package by.seal.greetings;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {
	private static final String template = "Hello, %s!";

	@GetMapping("/greeting")
	public String sendGreeting(@RequestParam(value = "name", defaultValue = "world") String name) {
		return String.format(template, name);
	}
}
